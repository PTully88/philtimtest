Cypress.Commands.add('VerifyText', (element, text) => {
    cy.get(element).should('have.text', text);
})