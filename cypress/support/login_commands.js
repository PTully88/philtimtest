
    // "userName": "registeredUser@test.com",
    // "password": "12345"


Cypress.Commands.add('Login', () => {
    cy.get('#email').type("registeredUser@test.com")
    cy.get('#passwd').type("12345")
    cy.get('.form-group.form-ok')
    cy.get('#SubmitLogin > span').click()
    cy.VerifyText('.page-heading', 'My account')
})